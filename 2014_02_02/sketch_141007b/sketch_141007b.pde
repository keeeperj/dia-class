PImage img;

color[] palette = new color[256];
int r = 108;
int g = 254;
int b = 101;

boolean t = true;

void setup() {
  img = loadImage("face_w.jpg");
  size(img.width, img.height);
  image(img, 0, 0);

  for (int i=0; i<256; i++) {
    palette[i] = color(r*i/255, g*i/255, b*i/255);
  }
}

void draw() {
  for (int y=0; y<height; y++) {
    for (int x=0; x<width; x++) {
      int index = x+y*width;
      color c = img.pixels[index];
      float newIndex = brightness(c);
      noStroke();
      fill(palette[int(newIndex)]);
      rect(x, y, 1, 1);
    }
  }
}

void keyPressed() {
  t = !t;
  switchPalette();
}

void switchPalette() {
  if (t == true) {
    for (int i=0; i<256; i++) {
      palette[i] = color(r*i/255, g*i/255, b*i/255);
    }
  } else {
    for (int i=0; i<256; i++) {
      if (i<180) {
        palette[i] = color(255*i/255, 0, 0);
      } else if (i>=180 && i<235) {
        palette[i] = color(0, 255*i/255, 0);
      } else {
        palette[i] = color(0, 0, 255*i/255);
      }
    }
  }
}

