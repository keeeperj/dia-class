PImage img;
int pixelSize = 1;

float worldRecordR = 0;
float worldRecordG = 0;
float worldRecordB = 0;
float worldRecordBR = 0;

int[] brArray;

void setup() {
  size(450, 331);
  brArray = new int[256];
  for (int i = 0; i<256; i++) {
    brArray[i] = 0;
  }
  img = loadImage("festivalsmal.jpg");

  for (int y=0; y<height; y+=pixelSize) {
    for (int x = 0; x<width; x+=pixelSize) {
      int index = x + y*width;
      color c = img.pixels[index];
      float r = red(c);
      float g = green(c);
      float b = blue(c);
      float br = brightness(c);
      //fill(c);
      //noStroke();
      //rect(x, y, pixelSize, pixelSize);
      /*if (r >= worldRecordR) {
        worldRecordR = r;
      }
      if (g >= worldRecordG) {
        worldRecordG = g;
      }
      if (b >= worldRecordB) {
        worldRecordB = b;
      }
      if (br >= worldRecordBR) {
        worldRecordBR = br;
      }*/
      brArray[int(br)]  = brArray[int(br)] + 1;
    }
  }
  
  int counterX = 0;
  int counterY = 0;
  
  for (int i=0; i<256; i++) {
    fill(i);
    noStroke();
    for (int k=0; i < brArray[i]; k++) {
      rect(counterX, counterY, 1, 1);
      counterX++;
      if(counterX > width){
        counterX = 0;
        counterY++;
      }
    }
  }
}

void draw() {
}
