class Ball {
  float x, y;
  float accX, accY;
  float speedX, speedY;
  int dirX, dirY;
  float dia;

  float maxSpeed;

  int lifeTime;
  int life;

  int type;

  Ball(int _type) {
    x = width/2;
    y = height/2;
    speedX = 0;
    speedY = 0;
    accX = random(-0.05, 0.05);
    accY = random(-0.06, 0.06);
    dirX = 1;
    dirY = 1;
    dia = 30;
    maxSpeed = random(6, 9);
    lifeTime = 0;
    life = int(random(300, 600));
    type = _type;
  }

  void update() {
    speedX += accX;
    speedY += accY;
    x += dirX*speedX;
    y += dirY*speedY;
    if (x > width-dia/2) {
      x = width-dia/2;
      dirX = dirX*-1;
    }
    if (x < dia/2) {
      x = dia/2;
      dirX = dirX*-1;
    }
    if (y > height-dia/2) {
      y = height-dia/2;
      dirY = dirY*-1;
    }
    if (y < dia/2) {
      y = dia/2;
      dirY = dirY*-1;
    }
    if (speedX > maxSpeed) {
      speedX = maxSpeed;
    } else if (speedX < -maxSpeed) {
      speedX = -maxSpeed;
    }
    if (speedY > maxSpeed) {
      speedY = maxSpeed;
    } else if (speedY < -maxSpeed) {
      speedY = -maxSpeed;
    }

    lifeTime++;
  }

  void display() {
    if (type == 0) {
      fill(255);
      text("0", x, y+dia);
      noFill();
      stroke(255);
      ellipse(x, y, dia, dia);
    } else if (type == 1) {
      fill(255);
      text("1", x, y+dia);
      fill(255);
      noStroke();
      ellipse(x, y, dia, dia);
    } else if (type == 2) {
      fill(255);
      text("2", x, y+dia);
      noFill();
      stroke(255);
      rect(x-dia/2, y-dia/2, dia, dia);
    } else if (type == 3) {
      fill(255);
      text("3", x, y+dia);
      fill(255);
      noStroke();
      rect(x-dia/2, y-dia/2, dia, dia);
    } else if (type == 4) {
      fill(255);
      text("4", x, y+dia);
      fill(255);
      noStroke();
      rect(x-dia/2, y-dia/2, dia, dia);
      fill(0);
      ellipse(x, y, dia, dia);
    }
  }

  boolean isDead() {
    if (lifeTime > life) {
      return true;
    } else {
      return false;
    }
  }
}

